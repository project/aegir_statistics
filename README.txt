This module provides a configurable block that contains statistics on the
number of sites, platforms and clients in the current Aegir instance. It's
meant to provide a simple, basic way of showing hosting statistics in a single
block.

Planned features:
 -Unlimited number of blocks
 -hooks to affect the different available statistics
 -More kinds of statistics blocks on more components (servers, users, quotas,
  averages)
 -Integration with remote servers
 -Email reports

This module needs to be installed on a hostmaster instance. It is not meant to
provide statistics on the current host from just any Drupal site (see Hosting
Diagnostics for that).

This module developed by and for koumbit.org, maintained by koumbit.org
and openatria.com
